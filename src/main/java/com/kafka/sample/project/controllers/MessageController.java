package com.kafka.sample.project.controllers;

import com.kafka.sample.project.dto.Address;
import com.kafka.sample.project.dto.MessageRequestDto;
import com.kafka.sample.project.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("message")
public class MessageController {

  @Autowired private KafkaTemplate<Long, UserDto> kafkaTemplate;

  @PostMapping
  public void sendOrder(@RequestBody MessageRequestDto msg) {
    msg.getUser().setAddress(new Address("country", "city", "street", 1L, 1L));
    ListenableFuture<SendResult<Long, UserDto>> future =
        kafkaTemplate.send("message", msg.getMessageId(), msg.getUser());
    future.addCallback(System.out::println, System.err::println);
    kafkaTemplate.flush();
  }
}
