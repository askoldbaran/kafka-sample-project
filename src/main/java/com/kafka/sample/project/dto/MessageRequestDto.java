package com.kafka.sample.project.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
@AllArgsConstructor
public class MessageRequestDto {

    private Long messageId;
    private UserDto user;

}
