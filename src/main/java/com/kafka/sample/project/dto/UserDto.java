package com.kafka.sample.project.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class UserDto {
    private Long age;
    private String name;
    private Address address;
}
